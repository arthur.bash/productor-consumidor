# Productor-consumidor


Este es un proyecto inicial en el cual defino:

1 consumidor el cual imprime por terminal El topic , la particion, la clave y el valor obtenido . Se queda esperando e imprimiento.


2 productores. 

Productor comun : En el cual Se define un host, un toppic una clave y un valor a enviar. y si envia normalmente. 

Productor con callback:  Este es una replica del anterior con la particularidad  de que devuelve un mensaje de error en caso de no poder enviarse el mensaje 

## Para iniciar proyecto:

Es necesario tener instalado localmente kafka.
Se puede descargar de: https://kafka.apache.org/downloads
Descomprimir el archivo.

### Para arrancar a usarlo es necesario ejecutar el kafka-zookeper:
```
    ./bin/zookeeper-server-start.sh ../config/zookeeper.properties 
```

### Crear un broker 
```
    ./bin/kafka-server-start.sh ../config/server.properties 
```

### Crear un toppic llamado 'topic-test'  
```
    ./bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 2 --topic topic-test
```

## A su vez tambien se puede crear un consumidor y productor por terminal, para chequear el funcionamiento:

### Consumidor 
    ./bin/kafka-console-consumer.sh  --bootstrap-server localhost:9092 --topic topic-test --group grupo1

### Productor
    ./bin/kafka-console-producer.sh  --bootstrap-server localhost:9092,localhost:9093 --topic topic-test
